package com.bunchhen.hw01;

import androidx.annotation.RequiresPermission;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class CreateUserActivity extends AppCompatActivity {

    User user = new User();

    // one text view
    TextView createUser;

    // four text fields
    EditText fullName, email, password, confirmPassword;

    // two buttons
    Button btnCancel, btnSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_user);

        // register all the EditText fields with their IDs.
        createUser = findViewById(R.id.createNewUser);
        fullName = findViewById(R.id.fullName);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        confirmPassword = findViewById(R.id.confirmPassword);

        // register buttons with their proper IDs.
        btnCancel = findViewById(R.id.btnCancel);
        btnSave = findViewById(R.id.btnSave);

        // handle Save button
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                isAllFieldsChecked = CheckAllFields();

                if (isAllFieldsChecked) {
                    Intent i = new Intent(getApplicationContext(), ProfileDetailsActivity.class);

                    // get value from editText field into object
                    user.setFullName(fullName.getText().toString());
                    user.setEmail(email.getText().toString());
                    user.setPassword(password.getText().toString());

                    // get data from object
                    i.putExtra("name", fullName.getText().toString());
                    i.putExtra("email", email.getText().toString());
                    i.putExtra("password", password.getText().toString());

                    startActivity(i);

                }

            }
        });

    }

    // validation all text fields and confirm password
    boolean isAllFieldsChecked = false;

    //validate email
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    private boolean CheckAllFields() {

        if (fullName.getText().toString().equals("")) {
            fullName.setError("Full name is required");
            return false;
        }

        if (email.getText().toString().equals("")) {
            email.setError("Email is required");
            return false;
        } else if (!email.getText().toString().matches((emailPattern))){
            email.setError("Invalid Email!");
            return false;
        }

        if (password.getText().toString().equals("")) {
            password.setError("Password is required");
            return false;
        }

        if (confirmPassword.getText().toString().equals("")) {
            confirmPassword.setError("Confirm password is required");
            return false;
        }

        if (!password.getText().toString().equals(confirmPassword.getText().toString())) {
            confirmPassword.setError("Password not match");
            return false;
        }

        return true;
        // after all validation return true.

    }
}